#!/bin/bash
weekday=`date +%a`
sudo -i -u cvfcc<<EOF
shopt -s nocasematch
for iterations in {1..10}
do
  cvmfs_server transaction fcc.cern.ch
  if [ "\$?" == "1" ]; then
    if  [[ "\$iterations" == "10" ]]; then
      echo "Too many tries... "

      # After 10 tries we stop trying
      echo "On-going transaction: retying later ..."
      exit 0

    else
       echo "Transaction is already open. Going to sleep..."
       sleep 2m
    fi
  else
    break
  fi
done

srcdir=/afs/cern.ch/work/f/fccsw/public/FCCDicts
dstdir=/cvmfs/fcc.cern.ch
echo "\$srcdir"
if test -d \$srcdir ; then
   echo "Running:"
   echo "           rsync -rvt \$srcdir \$dstdir"
   rsync -rvt \$srcdir \$dstdir
   if test "x\$?" == "x0" ; then
      echo "FCCDicts synchronization successful"
      echo "Finalizing publication ..."
      cvmfs_server publish fcc.cern.ch
      echo "... done!"
      exit 0
   else
      echo "FCCDicts synchronization failed!"
      echo "Aborting transaction ..."
      cvmfs_server abort -f fcc.cern.ch
      echo "... done!"
      exit 1
   fi
else
   echo "Source dir \$srcdir unavailble!"
   echo "Aborting transaction ..."
   cvmfs_server abort -f fcc.cern.ch
   echo "... done!"
   exit 1
fi
EOF
